# Gatsby Source IMDB

![Build Status](https://gitlab.com/ayyi.org/gatsby-source-imdb/badges/master/pipeline.svg)

![Screenshots](https://gitlab.com/ayyi.org/gatsby-source-imdb/-/raw/master/screenshots.webp "Screenshots")

The gatsby-source-imdb produces a set of IMDB data nodes using the 'imdb' package.

A copy of the thumbnail image is cached locally and is available either in an allFile query,
or as a child of the imdbNode

A list of IMDB ID's can be specified in gatsby-config.js

```javascript
module.exports = {
  plugins: [
    {
      resolve: 'gatsby-source-imdb',
      options: {
        items: [
          'tt0120157',
          'tt2414766'
        ]
      }
    }
  ]
}
```

To consume the data, an example query would be:
```graphql
query MyQuery {
  allImdbNode(filter: {title: {eq: "Some Movie"}}) {
    nodes {
      title
      description
      year
      rating
      children {
        ... on File {
          publicURL
        }
      }
    }
  }
}
```

To have cached data that is more persistent than the Gatsby cache, use the variable
"cacheDir" in gatsby-config.js to specify where the data is to be stored.
