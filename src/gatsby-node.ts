import fs from 'fs'
import path from 'path'
import util from 'util'
import http from 'http'
import https from 'https'
import bl from 'bl'
import imdb from 'imdb'
import { GatsbyNode, GatsbyCache, PluginOptions, SourceNodesArgs } from 'gatsby'

const { createFileNodeFromBuffer } = require('gatsby-source-filesystem')

export interface ImdbNode {
  id: string
  buffer?: Buffer
  [propName: string]: any
}

interface ImdbOptions extends PluginOptions {
  items: Array<string>
  cacheDir?: string
}

/*
 *  Create Gatsby nodes based on a list of IMDB id's in gatsby-config
 *  The nodes are available at 'allImdbNode'
 */
const sourceNodes: GatsbyNode['sourceNodes'] = async (
  { actions: {createNode}, createNodeId, getCache, createContentDigest, cache }: SourceNodesArgs,
  { items, cacheDir }: ImdbOptions
) => {

  if(!items)
    throw Error(`Please supply list of imdb id's`)

  const reduced: Array<ImdbNode> = await items.reduce(
    async (acc, id: string) => {
      const results = await acc

      return [
        ...results,
        {
          ...(await cache.get(id) || await load_data(id, cache, cacheDir))
        }
      ]
    },
    Promise.resolve(<ImdbNode[]>[])
  )

  for await (const nodeData of reduced) {
    const id = nodeData.id

    const thumbnail = nodeData.buffer
      ? await createFileNodeFromBuffer({
          name: id,
          buffer: nodeData.buffer,
          parentNodeId: id,
          getCache: getCache,
          createNode: createNode,
          createNodeId: createNodeId,
        })
      : undefined

    createNode({
      ...nodeData,
      children: thumbnail && [thumbnail.id],
      id,
      internal: {
        type: 'ImdbNode',
        contentDigest: createContentDigest(nodeData),
      }
    })
  }
}
exports.sourceNodes = sourceNodes

async function load_data (id: string, cache: GatsbyCache, cacheDir?: string) {
  let data

  const filename = path.resolve(`${cacheDir}/${id}.json`)

  const data_from_net = async (id: string, cache: GatsbyCache) => {
    data = await util.promisify(imdb)(id)
    if(data){
      if(data.poster == 'N/A')
        data.poster = null

      await cache.set(id, data)
      fs.writeFileSync(filename, JSON.stringify(data))
      if(data.poster){
        data.buffer = await url_to_array(data.poster)
        if(cacheDir){
          await save_buffer_image(data.buffer, path.resolve(`${cacheDir}/${id}.jpg`))
        }
      }
    }
  }

  if (await file_exists(filename)) {
    data = JSON.parse('' + await fs.promises.readFile(filename))
    data.id = id
    await cache.set(id, data)
    try {
      data.buffer = await util.promisify(fs.readFile)(`${cacheDir}/${id}.jpg`)
    } catch (e) {
      console.warn('no imdb thumbnail')
    }
  } else {
    data = data_from_net(id, cache)
  }

  return data
}

function file_exists (filename: string) {
  return new Promise(resolve => {
    fs.access(filename, async (err) => {
      resolve(!err)
    })
  })
}

function save_buffer_image (buffer: Buffer, path: string) {
  return util.promisify(fs.writeFile)(path, buffer, 'binary')
}

function url_to_array (url: string) {
  return new Promise((resolve, reject) => {
    https.get(url, (response: http.IncomingMessage) => {
      response.pipe(new bl((err: Error, data: Buffer) =>
        err ? reject(err) : resolve(data)
      ))
    })
  })
}
