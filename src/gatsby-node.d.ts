
export interface ImdbNode {
  id: string
  buffer?: Buffer
  [propName: string]: any
}

