import fs from 'fs'
import { Node, Actions, NodePluginArgs, Reporter, Store, GatsbyCache } from 'gatsby'
import { ImdbNode } from '../gatsby-node'

const { sourceNodes } = require('../gatsby-node')
const { objectContaining } = expect

interface NodeDict {
  [index: string]: Node
}

const nodes : NodeDict = {}

jest.mock(`gatsby-source-filesystem`, () => {
  const id = 'file-node-1'
  return {
    createFileNodeFromBuffer: jest.fn(() => (nodes[id] = {id} as Node))
  }
})

describe('gatsby-plugin-imdb', () => {
  const createContentDigest = jest.fn().mockReturnValue(`contentDigest`)

  const actions : Partial<Actions> = {
    createNode: jest.fn(node => (nodes[node.id] = <Node>node))
  }

  const getNodes = jest.fn(() => {
    return Object.keys(nodes).map(key => nodes[key])
  })

  const reporter : Partial<Reporter> = {
    info: jest.fn(),
    log: jest.fn(),
  }

  const store : Partial<Store> = {
    getState: jest.fn(() => {
      return {
        status: {
          plugins: []
        }
      }
    })
  }

  const cache = {
    get: () => Promise.resolve(null),
    set: () => Promise.resolve({})
  }

  const args : Partial<NodePluginArgs> = {
    createContentDigest,
    actions: <Actions>actions,
    reporter: <Reporter>reporter,
    store: <Store>store,
    getNode: id => nodes[id] as Node,
    getNodes: getNodes,
    cache: <GatsbyCache>cache
  }

  const fixture = {
    id: 'tt0123571',
    data: <NodeDict>{}
  }

  beforeAll(async () => {
    await sourceNodes(args, {
      items: [fixture.id],
      cacheDir: `${__dirname}/../fixtures`
    })
  
    fixture.data = JSON.parse('' + await fs.promises.readFile(`${__dirname}/../fixtures/${fixture.id}.json`))
  })

  it('has nodes', () => {
    expect(Object.keys(nodes).length).not.toEqual(0)
    expect(nodes[fixture.id]).toBeDefined()
  })

  it(`contains contentDigest`, () => {
    expect(nodes[fixture.id]).toEqual(
      objectContaining({
        internal: objectContaining({ contentDigest: `contentDigest` })
      })
    )
  })

  it(`has correct properties`, () => {
    ['title', 'year', 'contentRating', 'runtime', 'description', 'rating', 'director', 'poster', 'genre', 'metascore']
      .forEach(v => expect(nodes[fixture.id][v]).toEqual(fixture.data[v]))
  })

  it(`has thumbnail`, () => {
    const node : ImdbNode = nodes[fixture.id]

    expect(nodes['file-node-1']).toBeDefined()
    expect(node.children).toBeDefined()
    expect((<Array<string>>node.children)[0]).toEqual('file-node-1')
    expect(node.buffer?.length).toEqual(1113)
  })
})
